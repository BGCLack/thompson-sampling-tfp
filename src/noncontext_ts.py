import tensorflow as tf
import tensorflow_probability as tfp

import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

import pandas as pd
import numpy as np
import math
import os

tfd = tfp.distributions

from NoContextBandit import AgentNoContext, BanditNoContext

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SRC_DIR = os.path.join(BASE_DIR, 'src')
PLOT_DIR = os.path.join(BASE_DIR, 'plots')

sns.set_style('darkgrid')

plt.rc('font', family='serif',size=24)
matplotlib.rc('text', usetex=True)
matplotlib.rc('legend', fontsize=24)
matplotlib.rcParams['text.latex.preamble'] = r'\boldmath'

def plot_posterior(axs, bandit, agent, iteration):
    lin_x = np.arange(0, 1, 0.001)

    for arm_id in range(bandit.num_arms):
        a = agent.hist_dict[arm_id][1] + 1 # successes + 1
        b = agent.hist_dict[arm_id][0] - agent.hist_dict[arm_id][1] + 1 # failures + 1
        y = tfd.Beta(
            concentration1=a, 
            concentration0=b, 
        ).prob(lin_x)
        
        mean = np.round((a + b)/b, 1)
        successes = agent.hist_dict[arm_id][1] 
        failures = agent.hist_dict[arm_id][0] - agent.hist_dict[arm_id][1] 
        
        text = f"""
        mean_prob: {mean}
        s = {successes}
        f = {failures}
        """
        
        sns.lineplot(x=lin_x, y=y, ax=axs[arm_id])
        axs[arm_id].set_xlabel("payout_prob")
        axs[arm_id].set_title(f"Arm {arm_id} \n pulls: {agent.hist_dict[arm_id][0]}, successes: {agent.hist_dict[arm_id][1]}")
        # ax[i].text(0.95, 0.05, text, horizontalalignment='left', verticalalignment='top',)
        # transform=ax[i].transAxes
        # Add in true value of probability
        axs[arm_id].vlines(x=bandit.arm_probs[arm_id], ymin=0, ymax=5, colors='r', linestyles='--')

def main():
    bandit = BanditNoContext(num_arms=4, arm_probs=[0.1, 0.3, 0.5, 0.8])
    agent = AgentNoContext(num_arms=bandit.num_arms)
    iterations=1001
    nrows=math.floor(iterations/100) + 1
    ncols=bandit.num_arms
    print(nrows)

    fig, axs = plt.subplots(nrows=nrows, ncols=1, constrained_layout=True, figsize=(ncols*6, nrows*6))
    fig.suptitle('Bernoulli agent updating')

    for ax in axs:
        ax.remove()

    # add subfigure per subplot
    gridspec = axs[0].get_subplotspec().get_gridspec()
    subfigs = [fig.add_subfigure(gs) for gs in gridspec]

    r=0
    for i in range(iterations):
        if i%100 == 0:
            print(r)
            subfig = subfigs[r]
            subfig.suptitle(f'Iterations: {i}')
            axs = subfig.subplots(nrows=1, ncols=ncols)
            plot_posterior(axs, bandit, agent, iteration=i)
            r += 1
        agent.action_agent(bandit=bandit)

    fig.savefig(os.path.join(PLOT_DIR, 'No_context_analytic_iter.png'))

def test():
    bandit = BanditNoContext(num_arms=4, arm_probs=[0.1, 0.3, 0.5, 0.8])
    print(bandit)

def plot_single_arm_dist():
    # Single arm bandit to demonstrate updating.
    bandit = BanditNoContext(num_arms=1, arm_probs=[0.8])
    agent = AgentNoContext(num_arms=bandit.num_arms)
    iterations = 501
    plot_points = [0, 10, 50, 100, 250, 500]

    nr, nc = (3, 2)
    fig, axs = plt.subplots(nrows=3, ncols=2, constrained_layout=True, figsize=(nc*4, nr*4))
    r, c = (0, 0)
    fig.suptitle("Bayesian Updating of probability")
    for n in range(iterations):
        if n in plot_points:
            print(f"r={r}, c={c}")
            agent.plot_prob_dist(ax=axs[r,c], arm_id=0, bandit=bandit)
            c += 1
            if c%2 == 0:
                r += 1
            c = c%2
        agent.action_agent(bandit=bandit)

    fig.savefig(os.path.join(PLOT_DIR, 'single_arm_updating.png'))

if __name__ == '__main__':
    main()
    # plot_single_arm_dist()
    # test()