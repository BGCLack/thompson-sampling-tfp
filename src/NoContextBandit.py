import pandas as pd
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

import matplotlib.pyplot as plt
import seaborn as sns

tfd = tfp.distributions
tf.stop_gradient


class BanditNoContext():
    """
    Non contextual Bernoulli bandit class which is instantiated with stationary 
    probabiltities for each each arm

    Keyword Arguments:
        num_arms (int) - Number of arms the bandit has
        arm_probs (list) - List of true payout probabilities (0,1) that each arm
            has. Randomized if None.
    """
    def __init__(self, num_arms = 3, arm_probs = None):
        super(BanditNoContext, self).__init__()
        self.num_arms = num_arms
        self.arm_probs = arm_probs

        if isinstance(self.arm_probs, list):
            self.arm_probs = np.array(self.arm_probs)

        if self.arm_probs is None:
            self.arm_probs = np.round_(
                [np.random.uniform(0, 1) for i in range(self.num_arms)], 
                decimals=1
            )
    
    def action_arm(self, arm_id):
        """
        Pull an arm of the bandit and return the reward.

        Keyword Arguments:
            arm_id (int): Arm of bandit to pull.

        Returns
            reward: Binary value of the reward.
        """

        # Sample a Bernoulli distribution given the true probability of the 
        # arm
        reward = tfd.Bernoulli(
                probs=self.arm_probs[arm_id],
                dtype=tf.float32,
                name='Bernoulli'
            ).sample().numpy()
        return reward

    def __repr__(self):
        return np.array_str(self.arm_probs)

class AgentNoContext():
    """
    Agent which learns the probabilities and explores and exploits the arms of 
    an associated bandit.

    Keyword Arguments:
        num_arms (int) - Number of arms to sample, explore and action. Should 
            match the number of possibiliities of the bandit.
        hist_dict (dict) - Dictionary of agents history with the bandit. Tracks 
            the total number of attempts and successes for each arm.
        total_reward (int) - Total number of successes across all arms.
        total_attempts (int) - Total number of attempts across all arms.
    """
    def __init__(self, num_arms):
        super(AgentNoContext, self).__init__()
        self.num_arms = num_arms
        self.hist_dict = {}
        for i in range(num_arms):
            self.hist_dict[i] = [0, 0]
        self.total_reward = 0
        self.total_attempts = 0


    def sample_arms(self):
        """
        Sample the each arm given the most recent prior distribution of the 
        probabilities. This is modeled as a Beta function. 

        Returns: The id of the arm with the highest sampled probability of 
        payout.
        """

        samps = [] # Store samples in list
        for i in range(self.num_arms):
            # Define beta function based on historical successes.
            s = tfd.Beta(
                concentration1 = self.hist_dict[i][1] + 1, # a
                concentration0 = self.hist_dict[i][0] - self.hist_dict[i][1] + 1, # b
                force_probs_to_zero_outside_support=True,
                name = 'beta'
            ).sample().numpy()
            samps.append(s)
        
        return np.argmax(samps)

    def action_arm(self, arm_id, bandit):
        """
        Interact with the bandit. Select the arm to pull and recieve the 
        associated reward.

        Keyword Arguments:
        arm_id (int) - arm of bandit to sample.
        bandit (Bandit class) - Instantiated bandit class to interact with.
        """
        return bandit.action_arm(arm_id)

    def action_agent(self, bandit):
        """
        Perform an action with the agent. This includes sampling, pulling the 
        arm, and updating the statistics.

        Keyword Arguments:
        bandit (Bandit class) - Instantiated bandit class to interact with.
        """
        # print(self.hist_dict)
        arm_id = self.sample_arms()
        reward = self.action_arm(arm_id, bandit)
        self.total_reward += reward
        self.total_attempts += 1

        self.hist_dict[arm_id][1] += reward
        self.hist_dict[arm_id][0] += 1

    def plot_prob_dist(self, ax, arm_id, bandit=None,):
        lin_x = np.arange(0, 1, 0.001)
        a = self.hist_dict[arm_id][1] + 1 # successes + 1
        b = self.hist_dict[arm_id][0] - self.hist_dict[arm_id][1] + 1 # failures + 1
        y = tfd.Beta(
            concentration1=a, 
            concentration0=b, 
            ).prob(lin_x)

        mean = np.round((a + b)/b, 1)
        successes = self.hist_dict[arm_id][1] 
        failures = self.hist_dict[arm_id][0] - self.hist_dict[arm_id][1]
        text=f"""
        mean = {mean} 
        """

        # fig, ax = plt.subplots()
        sns.lineplot(x=lin_x, y=y, ax=ax, label='Agent distribution')
        ax.set_xlabel("payout_prob")
        ax.set_title(f"Attempts: {self.hist_dict[arm_id][0]}, Successes: {self.hist_dict[arm_id][1]}")
        # ax.text(0.95, 0.05, text, horizontalalignment='left', verticalalignment='top',)
        # plt.text(.01, .99, text, ha='left', va='top')
        # transform=ax[i].transAxes
    
        if isinstance(bandit, BanditNoContext):
            ax.vlines(x=bandit.arm_probs[arm_id], ymin=0, ymax=5, colors='r', linestyles='--', label="True Prob")
        ax.legend(loc='upper left')



    def __repr__(self):
        print(f"Total Reward: {self.total_reward}")
        print(f"Total Attempts: {self.total_attempts}")
        return repr(
            pd.DataFrame.from_dict(
                self.hist_dict, 
                orient='index', 
                columns=['attempts', 'successes']
            )
        )




